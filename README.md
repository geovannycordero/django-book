# Django training with the book Django For Professionals

## Production websites with Python & Django book, by William S. Vincent

## Set Up
> All the initial set up to start with Django are described in this [page](https://djangoforbeginners.com/initial-setup/).

1. Python
The book use the version 3.7. Youu can find information about Python installation in his [official page](https://www.python.org/downloads/).

2. Virtual Environment
Virtual environments are an indispensable part of Python programming. They are an isolated container containing all the software dependencies for a given project. In this case we will use `Pipenv` to manage virtual environments.
Use the next commando to install it.
```bash
pip3 install pipenv
```

3. Docker
Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Go to the [official site](https://docs.docker.com/) and follow the adequate instructions to install it in your machine.
Check your docker version.
```bash
docker --version
```
Additionally, you can run the Docker Hello World to ensure that all is correctly installed.
```bash
docker run hello-world
```

4. PostgreSQL
We will use Postgres 11 in a Docker container, there is no need that you install it in your local machine. To pull the image from the Docker official repository, run the next command.
```bash
docker pull postgres:11
```

When you have created a project, update the `settings.py` file to use PostgreSQL like your database. Django use by default MySQL.
Edit the file `settings.py` in the DATABASES section.

```python
DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.postgresql',
    'NAME': 'postgres',
    'USER': 'postgres',
    'PASSWORD': 'postgres',
    'HOST': 'db',
    'PORT': 5432
  }
}
```

Then, you will need [Psycopg](http://initd.org/psycopg/), this is a database adapter for Python. On the command line, enter the following command so it is installed within our Docker host.
```bash
docker-compose exec web pipenv install psycopg2-binary==2.8.3
```

If you have previously run the docker container, you need to rebuild the image. Stop docker compose and run the following command.
```bash
docker-compose up -d --build
```

## Run

1. Django Hello World
Create a Django “Hello, World” project that runs locally on our computer. Then we would move it entirely within Docker.

```bash
$ mkdir hello && cd hello
$ pipenv install django==2.2.7
$ pipenv shell
(hello) $
(hello) $ django-admin startproject hello_project .
(hello) $ python manage.py migrate
(hello) $ python manage.py runserver
```

Now, you should be able to navigate to http://127.0.0.1:8000/ in your web browser and be able to see the Django Welcome page.

2. Django on Docker
A Dockerfile represents a snapshot in time of what a project contains, what is called Docker image. Then, a Docker container is a running instance of an image.

Create your first Dockerfile and put the next code inside it.
```dockerfile
# Pull base image
FROM python:3.7

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /code

# Install dependencies
COPY Pipfile Pipfile.lock /code/
RUN pip install pipenv && pipenv install --system

# Copy project
COPY . /code/
```
Build the image using the next command.
```bash
docker build .
```

Now, need to create a `docker-compose.yml` file to control how to run the container that will be built based upon our Dockerfile image. Create the file and set the next content on it.

```yml
version: '3.7'

services:
  web:
    build: .
    command: python /code/manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/code
    ports:
      - 8000:8000
    depends_on:
      - db
  db:
    image: postgres:11
```

Run the Docker container using docker compose with the next command.
```bash
docker-compose up
```

Go back to http://127.0.0.1:8000/ in your web browser, refresh the page and the “Hello, World” page should still appear.

You can run docker compose in detach mode (run container in the background) and continue using the same console to the next steps with the command:
```bash
docker-compose up -d
```

The database is empty, we need both migrate and create a superuser within Docker that
will access the PostgreSQL database.
```bash
$ docker-compose exec web python manage.py migrate
$ docker-compose exec web python manage.py createsuperuser
```

You will be asked for some data, set that and now go to [http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/) in your browser. Enter the new superuser log in information and everything should be working.
